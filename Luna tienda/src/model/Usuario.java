package model;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;
import java.util.List;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Usuario {

	@PrimaryKey
	@Persistent
	private String email;
	
	@Persistent
	private String password;
	
	@Persistent
	private String name;
	
	@Persistent
	private char userType;
	
	public Usuario(String email, String password, String name, char type) {
		super();
		this.email = email;
		this.name = name;
		this.password = password;
		this.userType = type;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}
	
	public String getUserType() {
		String type = "Administrador";
		if(userType == 'U'){
			type = "Usuario";
		}
		return type;
	}
	
	public char getType() {
		return userType;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public void setUserType(char type) {
		this.userType = type;
	}
}
