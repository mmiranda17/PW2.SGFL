package model;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;
import java.util.List;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Producto {

	@PrimaryKey	
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Long codigo;
	
	@Persistent
	private String nombre;
	
	@Persistent
	private String descripcion;
	
	@Persistent
	private double precio1;
	
	@Persistent
	private double precio2;
	
	@Persistent
	private char estado;
	
	@Persistent
	private String marca;
	
	@Persistent
	private String tipoType;
	
	@Persistent
	private String foto1;
	
	@Persistent
	private String foto2;
	
	public Producto(String nombre, String descripcion, double precio1, double precio2
					,char estado, String marca, String tipoType, String foto1, String foto2){
		super();
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.precio1 = precio1;
		this.precio2 = precio2;
		this.estado = 'A';
		this.marca = marca;
		this.tipoType = tipoType;
		this.foto1 = foto1;
		this.foto2 = foto2;
	}
	
	public Long getCodigo(){
		return codigo;
	}
	public void setCodigo(Long codigo){
		this.codigo = codigo;
	}
	
	public String getNombre(){
		return nombre;
	}	
	public void setNombre(String nombre){
		this.nombre=nombre;
	}
	
	public String getDescripcion(){
		return descripcion;
	}
	public void setDescripcion(String descripcion){
		this.descripcion=descripcion;
	}
	
	public double getPrecio1(){
		return precio1;
	}
	public void setPrecio1(double precio1){
		this.precio1=precio1;
	}
	
	public double getPrecio2(){
		return precio2;
	}
	public void setPrecio2(double precio2){
		this.precio2=precio2;
	}
	
	public char getEstado(){
		return estado;
	}
	public void setEstado(char estado){
		this.estado=estado;
	}
	
	public String getMarca(){
		return marca;
	}
	public void setMarca(String marca){
		this.marca=marca;
	}
	
	public String getTipoType(){
		return tipoType;
	}
	public void setTipoType(String tipoType){
		this.tipoType=tipoType;
	}
	
	public String getFoto1(){
		return foto1;
	}
	public void setFoto1(String foto1){
		this.foto1=foto1;
	}
	
	public String getFoto2(){
		return foto2;
	}
	public void setFoto2(String foto2){
		this.foto2=foto2;
	}
}
