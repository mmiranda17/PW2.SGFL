package model;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;
import java.util.List;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Servicio {
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Long codigo;
	
	@Persistent
	private String nombre;
	
	@Persistent
	private String descripcion;
	
	@Persistent
	private String foto;
	
	@Persistent
	private char tipoType;
	
	public Servicio(String nombre, String descripcion, String foto, char tipoType){
		super();
		this.nombre=nombre;
		this.descripcion=descripcion;
		this.foto=foto;
		this.tipoType=tipoType;
	}
	
	public Long getCodigo(){
		return codigo;
	}
	
	public String getNombre(){
		return nombre;
	}
	public void setNombre(String nombre){
		this.nombre=nombre;
	}
	
	public String getDescripcion(){
		return descripcion;
	}
	public void setDescripcion(String descripcion){
		this.descripcion=descripcion;
	}
	
	public String getFoto(){
		return foto;
	}
	public void setFoto(String foto){
		this.foto=foto;
	}
	
	public char getTipoType(){
		return tipoType;
	}
	public void setTipoType(char tipoType){
		this.tipoType=tipoType;
	}	
}
