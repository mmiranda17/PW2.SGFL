package manageUser;

import java.io.IOException;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.*;

import model.Usuario;
import model.PMF;

@SuppressWarnings("serial")
public class UpdateUser extends HttpServlet {
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("text/plain");
		
		String email = req.getParameter("email");
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		Usuario c = pm.getObjectById(Usuario.class, email);
		
		String message = null;

		try{
			req.setAttribute("usuario", c);
			RequestDispatcher rd = req.getRequestDispatcher("/Admin/pages/admin.jsp");
			rd.forward(req, resp);
		}catch(Exception e){
			resp.getWriter().println(e);
		}finally{
			pm.close();
		}
	}
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("text/plain");
		String 	email = req.getParameter("email");
		String 	password = req.getParameter("password");
		String 	name = req.getParameter("name");
		String 	type = req.getParameter("type");
		String 	scope = req.getParameter("interface");
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		String message = "";
		Usuario u = null;
		
		char userType = 'U';
		if(type.equals("Administrador"))
			userType = 'A';
		//Verificando si el usuario existe
		try{
			u = pm.getObjectById(Usuario.class, email);
			u.setName(name);
			u.setEmail(email);
			u.setUserType(userType);
			u.setPassword(password);
		}catch(Exception e){
			message = "Ocurri� un error al guardar los datos, vuelva a intentarlo.";
		}
		
		//Guardando el usuario
		try{
			pm.makePersistent(u);
			message="Se Registr� con exito.";
		}catch(Exception e){
			message="Ocurri� un error al guardar los datos, vuelva a intentarlo.";
		}finally{
			pm.close();
		}
	
		//redireccionando a su respectiva interface
		if(scope.equals("admin")){
			HttpSession session=req.getSession();
			session.setAttribute("message",message); 
			System.out.println(message);
			resp.sendRedirect("/ver_usuarios");
		}
		
		//Renderizar datos en index.jsp
		RequestDispatcher rd = req.getRequestDispatcher("/User/index.jsp");
		req.setAttribute("message", message);
		try{
			rd.include(req, resp);
		}catch(Exception e2){
			System.out.println(e2);
		}
	}
}