package manageUser;

import java.io.IOException;
import java.util.List;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.*;

import model.PMF;
import model.Usuario;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

@SuppressWarnings("serial")
public class ShowUsers extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setContentType("text/html");
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		final Query q = pm.newQuery(Usuario.class);
		
		try{
			List<Usuario> usuarios = (List<Usuario>) q.execute();
			req.setAttribute("usuarios", usuarios);
			RequestDispatcher rd = req.getRequestDispatcher("/Admin/admin.jsp");
			rd.forward(req, resp);
		}catch(Exception e){
			resp.getWriter().println(e);
		}finally{
			pm.close();
			q.closeAll();
		}
	}
}
