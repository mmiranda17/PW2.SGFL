package manageUser;

import java.io.IOException;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.*;

import model.Usuario;
import model.PMF;

@SuppressWarnings("serial")
public class DeleteUser extends HttpServlet {
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("text/plain");
		
		String email = req.getParameter("email");
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		Usuario c = pm.getObjectById(Usuario.class, email);
		
		String message = null;

		try{
			pm.deletePersistent(c);
			message = "Usuario eliminado.";
		}catch(Exception e){
			message = "No se pudo eliminar al usuario.";	
		}finally{
			pm.close();
		}
		
		HttpSession session=req.getSession();
		session.setAttribute("message", message);
		resp.sendRedirect("/ver_usuarios");
	}
}