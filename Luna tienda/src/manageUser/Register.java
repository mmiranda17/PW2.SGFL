package manageUser;

import java.io.IOException;

import javax.jdo.PersistenceManager;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.*;

import controller.correo.ControladorCorreo;
import model.Usuario;
import model.PMF;
import model.Correo;


@SuppressWarnings("serial")
public class Register extends HttpServlet {
	
	Correo c = new Correo();
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setContentType("text/html");
		
		String 	email = req.getParameter("email");
		String 	password = req.getParameter("password");
		String 	name = req.getParameter("name");
		String 	type = req.getParameter("type");
		String 	scope = req.getParameter("interface");
		
		
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		String message = "";
		Usuario u = null, nuevo = null;
		
		char userType = 'U';
		if(type.equals("Administrador"))
			userType = 'A';
		//Verificando si el usuario existe
		try{
			u = pm.getObjectById(Usuario.class, email);
			message = "El usuario ya Existe.";
		}catch(Exception e){
			nuevo = new Usuario(email, password, name, userType);
		}
		
		//Guardando el usuario
		if(u == null)
			try{
				pm.makePersistent(nuevo);
				message="Se Registr� con exito.";
				System.out.println("what: "+ email);
				
				enviarcorreo(email,"Creacion de Cuenta");
				
			}catch(Exception e){
				message="Ocurri� un error al guardar los datos, vuelva a intentarlo.";
			}finally{
				pm.close();
			}
	
		//redireccionando a su respectiva interface
		if(scope.equals("admin")){
			HttpSession session=req.getSession();
			session.setAttribute("message",message); 
			System.out.println(message);
			resp.sendRedirect("/ver_usuarios");
		}
		
		//Renderizar datos en index.jsp
		RequestDispatcher rd = req.getRequestDispatcher("/User/index.jsp");
		req.setAttribute("message", message);
		try{
			rd.forward(req, resp);
		}catch(Exception e2){
			System.out.println(e2);
		}
	}
	
	
	
	public void enviarcorreo(String para,String asunto){
		c.setContrasena("czppsyslauswbrle");
		c.setUsuarioCorreo("jmmiranda.c@gmail.com");
		c.setAsunto(asunto);
		c.setMensaje("SU CUENTA HA SIDO CREADA!");
		c.setDestino(para);
		
		ControladorCorreo co = new ControladorCorreo();
		co.enviarCorreo(c);
	}
}
