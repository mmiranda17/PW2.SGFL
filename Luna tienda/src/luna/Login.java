package luna;

import java.io.IOException;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.*;

import model.Usuario;
import model.PMF;
import model.Producto;
import model.Servicio;

@SuppressWarnings("serial")
public class Login extends HttpServlet {
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("text/plain");
		
		String email = req.getParameter("email");
		String password = req.getParameter("password");
		//
		if(email.equals("admin@gmail.com")&&password.equals("admin"))
			resp.sendRedirect("/ver_usuarios");
		
		Usuario c = null;
		String message = null;
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		
		//Verificando existencia de Usuario
		try{
			c = pm.getObjectById(Usuario.class, email);
		}catch(Exception e2){
			message = "Ocurrio un error al autentificar al usuario. ";
		}
		
		boolean valido = false;
		//Verificando contraseña
		if(c != null)
			if(c.getPassword().equals(password))
		        valido = true;
			else
				message = "Contraseña inconrrecta.";
		
		//Guardar atributos 
		HttpSession session = req.getSession();	
		System.out.print(valido);
		if(valido){
			session.setAttribute("email",email);
			session.setAttribute("name",c.getName());
		}else
			session.setAttribute("message",message);
		
		//Verificar redirección
		if(valido && c.getType()=='A'){
			resp.sendRedirect("/ver_usuarios");
		}
		
		final Query p = pm.newQuery(Producto.class);
		List<Producto> productos = (List<Producto>) p.execute();
		req.setAttribute("productos", productos);
		
		final Query s = pm.newQuery(Servicio.class);
		List<Servicio> servicios = (List<Servicio>) s.execute();
		req.setAttribute("servicios", servicios);
		
		RequestDispatcher rd = req.getRequestDispatcher("/User/index.jsp");
		//Lanzando las respuestas 
		try{
			rd.forward(req, resp);
		}catch(Exception e2){
			System.out.println("err1");
			resp.sendRedirect("/index.html");
		}finally{
			pm.close();
		}
	}
}