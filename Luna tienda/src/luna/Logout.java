package luna;

import java.io.IOException;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.*;

import model.Usuario;
import model.PMF;

@SuppressWarnings("serial")
public class Logout extends HttpServlet {
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("text/plain");
		
		HttpSession session=req.getSession();  
        session.invalidate();  
        
        resp.sendRedirect("/index.html");

	}
}