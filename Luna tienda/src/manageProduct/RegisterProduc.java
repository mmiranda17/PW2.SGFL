package manageProduct;

import java.io.IOException;

import javax.jdo.PersistenceManager;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.*;

import model.Producto;
import model.Usuario;
import model.PMF;

@SuppressWarnings("serial")
public class RegisterProduc extends HttpServlet{
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException{
		resp.setContentType("text/html");
		
		String nombre = req.getParameter("nombre");
		String descripcion = req.getParameter("descripcion");
		double precio1 = Double.parseDouble(req.getParameter("precio1"));
		double precio2 = Double.parseDouble(req.getParameter("precio2"));
		char estado = req.getParameter("estado").charAt(0);
		String marca = req.getParameter("marca");
		String tipoType = req.getParameter("tipoType");
		String foto1 = req.getParameter("foto1");
		String foto2 = req.getParameter("foto2");
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		String message = null;
		
		char tipo = 'A';
		if(tipoType.equals("Laptop"))
			tipo = 'L';
		Producto nuevo = new Producto(nombre, descripcion, precio1, precio2	,estado, marca, tipoType, foto1, foto2);		
		try{
			pm.makePersistent(nuevo);
			message="El producto a sido creado correctamente";
		}catch(Exception e){
			System.out.println(e);
			message="Ocurrio un proble al crear el producto";
		}finally{
			pm.close();
		}
		
		HttpSession session = req.getSession();
		session.setAttribute("message", message);
		resp.sendRedirect("/ver_productos");
		//Verificacion si el producto existe
		
		
		//Guardar producto
		
		
		//Rederizar datos en index.jsp
		
				
	}	
}
