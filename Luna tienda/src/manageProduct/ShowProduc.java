package manageProduct;

import java.io.IOException;
import java.util.List;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.*;

import model.PMF;
import model.Producto;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

@SuppressWarnings("serial")
public class ShowProduc extends HttpServlet{
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setContentType("text/html");
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		final Query q = pm.newQuery(Producto.class);
		
		try{
			List<Producto> productos = (List<Producto>) q.execute();
			req.setAttribute("productos", productos);
			RequestDispatcher rd = req.getRequestDispatcher("/Admin/productos.jsp");
			rd.forward(req, resp);
		}catch(Exception e){
			resp.getWriter().println(e);
		}finally{
			pm.close();
			q.closeAll();
		}
	}

}
