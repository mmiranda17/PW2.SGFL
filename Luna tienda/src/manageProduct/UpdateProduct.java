package manageProduct;

import java.io.IOException;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.*;

import model.Producto;
import model.PMF;

@SuppressWarnings("serial")
public class UpdateProduct extends HttpServlet{
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
		throws IOException {
		resp.setContentType("text/pain");
		
		Long codigo = Long.parseLong(req.getParameter("codigo"));
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		Producto p = pm.getObjectById(Producto.class, codigo);
		
		String message = null;
		try{
			req.setAttribute("form","producto");
			req.setAttribute("nombre", "Formulario Producto");
			req.setAttribute("producto", p);
			RequestDispatcher rd = req.getRequestDispatcher("/Admin/pages/forms.jsp");
			rd.forward(req, resp);
		}catch(Exception e){
			resp.getWriter().println(e);
		}finally{
			pm.close();
		}
	}
	
	public void  doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException{
		resp.setContentType("text/plain");
		
		Long codigo = Long.parseLong(req.getParameter("codigo"));
		String nombre = req.getParameter("nombre");
		String descripcion = req.getParameter("descripcion");
		double precio1 = Double.parseDouble(req.getParameter("precio1"));
		double precio2 = Double.parseDouble(req.getParameter("precio2"));
		char estado = req.getParameter("estado").charAt(0);
		String marca = req.getParameter("marca");
		String tipoType = req.getParameter("tipoType");
		String foto1 = req.getParameter("foto1");
		String foto2 = req.getParameter("foto2");

		
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		String message = "Hola Mundo XD";
		Producto p = null;
		
		char tipo = 'A';
		if(tipoType.equals("Laptop"))
			tipo = 'L';
		if(tipoType.equals("PC"))
			tipo = 'P';
		if(tipoType.equals("Tablet"))
			tipo = 'T';
		if(tipoType.equals("Celular"))
			tipo = 'C';
		
		try{
			p = pm.getObjectById(Producto.class, codigo);
			p.setNombre(nombre);
			p.setDescripcion(descripcion);
			p.setPrecio1(precio1);
			p.setPrecio2(precio2);
			p.setEstado(estado);
			p.setMarca(marca);
			p.setTipoType(tipoType);
			p.setFoto1(foto1);
			p.setFoto2(foto2);			
		}catch(Exception e){
			message = "Ocurrio un error al actualizar el producto";
		}
		
		try{
			pm.makePersistent(p);
			message="Se actualizo con exito el producto";
		}catch(Exception e){
			message = "Ocurrio un error al guardar los datos";
		}finally{
			pm.close();
		}
		
		HttpSession session=req.getSession();
		session.setAttribute("message", message);
		resp.sendRedirect("/ver_productos");
	}
}

