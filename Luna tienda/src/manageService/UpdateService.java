package manageService;

import java.io.IOException;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.*;

import model.Servicio;
import model.PMF;

@SuppressWarnings("serial")
public class UpdateService extends HttpServlet {
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("text/plain");
		
		Long codigo = Long.parseLong(req.getParameter("codigo"));
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		Servicio c = pm.getObjectById(Servicio.class, codigo);
		
		String message = null;
		try{
			req.setAttribute("form", "servicio");
			req.setAttribute("nombre", "Formulario Servicio");
			req.setAttribute("servicio", c);
			RequestDispatcher rd = req.getRequestDispatcher("/Admin/pages/forms.jsp");
			rd.forward(req, resp);
		}catch(Exception e){
			resp.getWriter().println(e);
		}finally{
			pm.close();
		}
	}
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("text/plain");
		
		Long codigo = Long.parseLong(req.getParameter("codigo"));
		String 	nombre = req.getParameter("nombre");
		String 	descripcion = req.getParameter("descripcion");
		String 	foto = req.getParameter("foto");
		String 	tipo = req.getParameter("tipo");
		String 	scope = req.getParameter("interface");
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		String message = "";
		Servicio s = null;
		
		char userType = 'M';
		if(tipo.equals("Asesoria"))
			userType = 'A';
		//Verificando si el usuario existe
		try{
			s = pm.getObjectById(Servicio.class, codigo);
			s.setNombre(nombre);
			s.setDescripcion(descripcion);
			s.setFoto(foto);
			s.setTipoType(userType);
		}catch(Exception e){
			message = "Ocurri� un error al guardar los datos, vuelva a intentarlo.";
		}
		
		//Guardando el usuario
		try{
			pm.makePersistent(s);
			message="Se Actualiz� con exito.";
		}catch(Exception e){
			message="Ocurri� un error al guardar los datos, vuelva a intentarlo.";
		}finally{
			pm.close();
		}
	
		//redireccionando a su respectiva interface
		if(scope.equals("admin")){
			HttpSession session=req.getSession();
			session.setAttribute("message",message); 
			resp.sendRedirect("/ver_servicios");
		}
		
		//Renderizar datos en index.jsp
		RequestDispatcher rd = req.getRequestDispatcher("/User/por_verse.jsp");
		req.setAttribute("message", message);
		try{
			rd.include(req, resp);
		}catch(Exception e2){
			System.out.println(e2);
		}
	}
}