package manageService;

import java.io.IOException;

import javax.jdo.PersistenceManager;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.*;

import model.Servicio;
import model.PMF;

@SuppressWarnings("serial")
public class ResgisterService extends HttpServlet{
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException{
		resp.setContentType("text/html");
		
		String nombre = req.getParameter("nombre");
		String descripcion = req.getParameter("descripcion");
		String foto = req.getParameter("foto");
		String tipoType = req.getParameter("tipo");
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		String message = null;
		
		char tipo = 'A';
		if(tipoType.equals("Mantenimiento"))
			tipo = 'M';
		Servicio nuevo = new Servicio(nombre, descripcion, foto, tipo);

		try{
			pm.makePersistent(nuevo);
			message="El servicio se registro con EXITO.";
		}catch(Exception e){
			System.out.println(e);
			message="Ocurrio un error al guaradar el producto";
		}finally{
			pm.close();
		}
		HttpSession session = req.getSession();
		session.setAttribute("message",message);
		resp.sendRedirect("/ver_servicios");
	}

}
