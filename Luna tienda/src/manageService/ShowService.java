package manageService;

import java.io.IOException;
import java.util.List;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.*;

import model.PMF;
import model.Servicio;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

@SuppressWarnings("serial")
public class ShowService extends HttpServlet{
	public void doGet(HttpServletRequest req, HttpServletResponse resp)throws IOException{
		resp.setContentType("text/html");
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		final Query q = pm.newQuery(Servicio.class);
		
		try{
			List<Servicio> servicios = (List<Servicio>) q.execute();
			req.setAttribute("servicios",  servicios);
			RequestDispatcher red = req.getRequestDispatcher("/Admin/servicios.jsp");
			red.forward(req,  resp);
		}catch(Exception e){
			resp.getWriter().println(e);
		}finally{
			pm.close();
			q.closeAll();
		}
	}

}
