package manageService;

import java.io.IOException;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.*;

import model.Servicio;
import model.PMF;

@SuppressWarnings("serial")
public class DeleteService extends HttpServlet{
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
	throws IOException{
		resp.setContentType("text/plain");
		
		Long codigo = Long.parseLong(req.getParameter("codigo"));		
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		Servicio s = pm.getObjectById(Servicio.class, codigo);
		
		String message = null;
		try{
			pm.deletePersistent(s);
			message = "Servicio Eliminado.";
		}catch(Exception e){
			message = "No se pudo eliminar el servicio. ";
		}finally{
			pm.close();
		}
		HttpSession session = req.getSession();
		session.setAttribute("message", message);
		resp.sendRedirect("/ver_servicios");
		
	}
}
