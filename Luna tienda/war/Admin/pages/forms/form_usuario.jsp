<%@ page import="model.Usuario"%>
<% Usuario u = (Usuario)request.getAttribute("usuario"); %>

	<div class="form-group col-sm-6">
       <label for="exampleInputEmail1">Nombre</label>
       <input name="name" class="form-control" id="exampleInputEmail1" placeholder="Nombre de usuario" type="text" value="<%if(u!= null){%><%=u.getName()%><%}%>">
   </div>
   <div class="form-group col-sm-6">
       <label for="exampleInputEmail1">Email address</label>
       <input name="email" class="form-control" id="exampleInputEmail1" placeholder="Correo electrónico" type="email" value="<%if(u!= null){%><%=u.getEmail()%><%}%>">
   </div>
   <div class="form-group col-sm-6">
       <label for="exampleInputEmail1">Contraseña</label>
       <input name="password" class="form-control" id="exampleInputEmail1" placeholder="Contraseña" type="password" value="">
   </div>
   <div class="form-group col-sm-6">
       <label>Permisos</label>
       <select name="type" class="form-control">
           <option>Usuario</option>
           <option>Administrador</option>
       </select>
   </div>
   <input name="interface" type="text" style="display:none;" value="admin">
    <div class="row">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-10">
            <input type="submit" class="btn btn-primary btn-sm" value="Guardar & Continuar" />
            <button type="button" class="btn btn-default btn-sm">Cancelar</button>
        </div>
    </div>