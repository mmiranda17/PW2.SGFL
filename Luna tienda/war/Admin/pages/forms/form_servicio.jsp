<%@ page import="model.Servicio"%>
<% Servicio s = (Servicio)request.getAttribute("servicio"); %>
	<div class="form-group">
        <label for="codigo" class="col-sm-2 control-label">
            Nombre</label>
        <div class="col-sm-10">
               <input type="text" name="nombre" class="form-control" placeholder="Nombre" value="<%if(s!= null){%><%=s.getNombre()%><%}%>" />	                                          
        </div>
    </div>
    
     <div class="form-group">
        <label for="precio2" class="col-sm-2 control-label">
            Descripción</label>
        <div class="col-sm-10">
               <input type="text" name="descripcion" class="form-control" placeholder="Descripción" value="<%if(s!= null){%><%=s.getDescripcion()%><%}%>" />	                                          
        </div>
    </div>
    
     <div class="form-group">
        <label for="estado" class="col-sm-2 control-label">
            Foto</label>
        <div class="col-sm-10">
            <input type="text" name="foto" class="form-control" placeholder="Url Foto" value="<%if(s!= null){%><%=s.getFoto()%><%}%>" />	                                          
        </div>
    </div>
    <div class="form-group">
        <label for="estado" class="col-sm-2 control-label">
            Tipo Servicio</label>
        <div class="col-sm-10">
            <select type="text" name="tipo" class="form-control">
	            <option>Mantenimiento</option>
	           	<option>Asesoria</option>	
            </select>                                          
        </div>
    </div>
    <input type="text" name="codigo" value="<%if(s!= null){%><%=s.getCodigo()%><%}%>" style="display:none;" />
    <input type="text" name="interface" value="admin" style="display:none;" />
    <div class="row">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-10">
            <input type="submit" class="btn btn-primary btn-sm" value="Guardar & Continuar" />
            <button type="button" class="btn btn-default btn-sm">Cancelar</button>
        </div>
    </div>



	                                
    
