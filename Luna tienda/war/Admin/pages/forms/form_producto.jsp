<%@ page import="model.Producto"%>
<% Producto p = (Producto)request.getAttribute("producto"); %>

	
     <div class="form-group">
        <label for="nombre" class="col-sm-2 control-label">
            Nombre</label>
        <div class="col-sm-10">
               <input type="text" name="nombre" class="form-control" placeholder="Nombre" value="<%if(p!= null){%><%=p.getNombre()%><%}%>" />	                                          
        </div>
    </div>
    
     <div class="form-group">
        <label for="descripcion" class="col-sm-2 control-label">
            Descripcion</label>
        <div class="col-sm-10">
               <input type="text" name="descripcion" class="form-control" placeholder="Descripcion" value="<%if(p!= null){%><%=p.getDescripcion()%><%}%>" />	                                          
        </div>
    </div>
    
     <div class="form-group">
        <label for="precio1" class="col-sm-2 control-label">
            Precio1</label>
        <div class="col-sm-10">
               <input type="text" name="precio1" class="form-control" placeholder="Precio1" value="<%if(p!= null){%><%=p.getPrecio1()%><%}%>" />	                                          
        </div>
    </div>
    
     <div class="form-group">
        <label for="precio2" class="col-sm-2 control-label">
            Precio2</label>
        <div class="col-sm-10">
               <input type="text" name="precio2" class="form-control" placeholder="Precio2" value="<%if(p!= null){%><%=p.getPrecio2()%><%}%>" />	                                          
        </div>
    </div>
    
     <div class="form-group">
        <label for="estado" class="col-sm-2 control-label">
            Estado</label>
        <div class="col-sm-10">
               <input type="text" name="estado" class="form-control" placeholder="Estado" value="<%if(p!= null){%><%=p.getEstado()%><%}%>" />	                                          
        </div>
    </div>
    
     <div class="form-group">
        <label for="marca" class="col-sm-2 control-label">
            Marca</label>
        <div class="col-sm-10">
               <input type="text" name="marca" class="form-control" placeholder="Marca" value="<%if(p!= null){%><%=p.getMarca()%><%}%>" />	                                          
        </div>
    </div>
    
     <div class="form-group">
        <label for="tipo" class="col-sm-2 control-label">
            Tipo</label>
        <div class="col-sm-10">
               <input type="text" name="tipoType" class="form-control" placeholder="Tipo" value="<%if(p!= null){%><%=p.getTipoType()%><%}%>" />	                                          
        </div>
    </div>
    
     <div class="form-group">
        <label for="foto1" class="col-sm-2 control-label">
            Foto1</label>
        <div class="col-sm-10">
               <input type="text" name="foto1" class="form-control" placeholder="Foto1" value="<%if(p!= null){%><%=p.getFoto1()%><%}%>" />	                                          
        </div>
    </div>
    
     <div class="form-group">
        <label for="foto2" class="col-sm-2 control-label">
            Foto2</label>
        <div class="col-sm-10">
               <input type="text" name="foto2" class="form-control" placeholder="Foto2" value="<%if(p!= null){%><%=p.getFoto2()%><%}%>" />	                                          
        </div>
    </div>		                                
    <input type="text" name="codigo" value="<%if(p!= null){%><%=p.getCodigo()%><%}%>" style="display:none;" />
    
    <div class="row">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-10">
            <input type="submit" class="btn btn-primary btn-sm" value="Guardar & Continuar" />
            <button type="button" class="btn btn-default btn-sm">
                Cancelar</button>
        </div>
    </div>