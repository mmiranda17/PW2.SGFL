<!-- ============================================================= -->
<!-- ===================== MESSAGE FIXDED ======================== -->
<!-- ============================================================= -->
<% String message = (String)request.getSession().getAttribute("message"); %>
<% if(message!=null){ %>
    <div class="alert alert-info alert-dismissable" style="position:fixed;top:90px;right:5px;">
        <i class="fa fa-info"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
        <%= message %>
    </div>
<%}%>