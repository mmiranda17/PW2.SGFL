<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="model.Usuario"%>
<%@ page import="model.Producto"%>
<%@ page import="java.util.List"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ferreteria Luna</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="/Admin/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="/Admin/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="/Admin/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="/Admin/css/morris/morris.css" rel="stylesheet" type="text/css" />
        <!-- jvectormap -->
        <link href="/Admin/css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
        <!-- fullCalendar -->
        <link href="/Admin/css/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
        <!-- Daterange picker 
        <link href="/Admin/css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
        -->
        <!-- bootstrap wysihtml5 - text editor -->
        <link href="/Admin/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="/Admin/css/AdminLTE.css" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-black">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="index.html" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                LunaAdmin
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <% String email = (String) request.getSession().getAttribute("email"); %>
                                <span><%= email %> <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">
                                    <img src="/Admin/img/avatar3.png" class="img-circle" alt="User Image" />
                                    <p>
                                        Administrador - General
                                        <small>Miembro desde Mayo 2017</small>
                                        <% String name = (String) request.getSession().getAttribute("name"); %>
                                        <%= name %>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-right">
					                    <a class="page-scroll" data-toggle="modal" data-target="#logout" style="cursor:pointer;">
					                    	Salir
					                    </a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        
        <div class="wrapper row-offcanvas row-offcanvas-left">
        	<!-- ============================================================= -->
        	<!-- ========================= leftside ========================== -->
        	<!-- ============================================================= -->
			<jsp:include page="/Admin/pages/Components/leftside.html" /> 
			
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Principal
                        <small>Panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Principal</a></li>
                        <li class="active">Panel</li>
                    </ol>
                </section>
				<%
					List<Producto> productos = (List<Producto>) request.getAttribute("productos");
				%>
				
				<!-- ======== ===================================== ======== -->
                <!-- ======== CONTENIDO PRINCIPAL LISTA DE PRODUCTOS ======== -->
                <!-- ======== ===================================== ======== -->
                <section class="content">
					<div class="box box-primary">
                        <div class="box-header" style="cursor: move;">
                            <i class="ion ion-clipboard"></i>
                            <h3 class="box-title">Lista de Productos</h3>
                            <div class="box-tools pull-right">
                                <ul class="pagination pagination-sm inline">
                                    <li><a href="#">
                                    	<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                    </a></li>
                                    <li><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">
                                    	<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                    </a></li>
                                </ul>
                            </div>
                        </div><!-- /.box-header -->
                              
                        <div class="box-body">
                       		<div style="margin-left:40px;width:150px;display:inline-block;">Nombre</div>
							<div style="margin-left:5px;width:150px;display:inline-block;">Descripción</div>
							<div style="margin-left:5px;width:50px;display:inline-block;">Precio1</div>
							<div style="margin-left:5px;width:50px;display:inline-block;">Precio2</div>
							<div style="margin-left:5px;width:50px;display:inline-block;">Estado</div>
							<div style="margin-left:5px;width:70px;display:inline-block;">Marca</div>
							<div style="margin-left:5px;width:70px;display:inline-block;">Tipo</div>
							<div style="margin-left:5px;width:100px;display:inline-block;">Foto1</div>
							<div style="margin-left:5px;width:100px;display:inline-block;">Foto2</div>
                            <ul class="todo-list ui-sortable">
                            	<% int i = 0; %>
                                <%for( Producto p : productos ) {%>
                                	<li>
                                		<!-- drag handle -->
                                        <span class="handle">
                                            <i class="fa fa-ellipsis-v"></i>
                                            <i class="fa fa-ellipsis-v"></i>
                                        </span> 
                                           
										<span class="text" style="width:150px;"><%= p.getNombre() %></span>
										<span class="text" style="width:150px;"><%= p.getDescripcion() %></span>
										<span class="text" style="width:50px;"><%= p.getPrecio1() %></span>
										<span class="text" style="width:50px;"><%= p.getPrecio2() %></span>										
										<span class="text" style="width:50px;"><%= p.getEstado() %></span>
										<span class="text" style="width:70px;"><%= p.getMarca() %></span>
										<span class="text" style="width:70px;"><%= p.getTipoType() %></span>
										<span class="text" style="width:100px;"><%= p.getFoto1() %></span>
										<span class="text" style="width:100px;"><%= p.getFoto2() %></span>

										<!-- General tools such as edit or delete-->
										<div class="tools">
                                            <a class="page-scroll" href="/editar_producto?codigo=<%= p.getCodigo() %>" style="cursor:pointer;">
                                            	<i class="fa fa-edit"></i>
                                            </a>
                                            <a class="page-scroll" data-toggle="modal" data-target="#modal<%=i%>" style="cursor:pointer;">
                                            	<i class="fa fa-trash-o"></i>
                                            </a>
                                       	</div>
									</li>
									
									<div class="modal fade" id="modal<%=i%>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
									    <div class="modal-dialog modal-lg">
									        <div class="modal-content">
									            <div class="modal-header">
									                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									                    X
									                </button>
									                <h4 class="modal-title" id="tit">
									                    ¿Está seguro que desea eliminar <b><%= p.getNombre() %></b>?
									                </h4>
									            </div>
									            <div class="modal-body" style="height:60px;">
									            	<a class="close" href="/eliminar_producto?codigo=<%= p.getCodigo() %>" style="opacity:1;">Eliminar</a>
									            	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="opacity:1;padding-right:40px;">
									                    Cancelar
									                </button>
									            </div>
									        </div>
									    </div>
									</div>
									<% i++; %>
                                <%}%>
                            </ul>
                        </div>
                    </div>

					<!-- Add message -->
					<jsp:include page="/Admin/pages/Components/message.jsp" /> 
					
					<!-- ============================================================= -->
					<!-- ===================== FORM Servicio ========================= -->
					<!-- ============================================================= -->
                    <div class="box box-primary collapsed-box" style="float: left;">
                        <div class="box-header" style="cursor: move;">
                            <!-- tools box -->
                            <div class="pull-right box-tools">
                                <button class="btn btn-default  pull-right" data-widget="collapse" data-toggle="tooltip" title="" style="margin-right: 5px;" data-original-title="+">
                                	<i class="fa fa-plus"></i>Agregar Producto
                                </button>
                            </div>
                            <h3 class="box-title">
                                
                            </h3>
                        </div>
                        <div class="box-body no-padding" style="display: none;">
                        	<h3 class="box-title" style="padding-left:15px;">
                                Formulario Producto
                            </h3>
                           	<form action="/registrar_producto" method="POST">    
			                    <!-- Add message -->
								<jsp:include page="/Admin/pages/forms/form_producto.jsp" /> 
							</form>
                        </div><!-- /.box-body-->
                    </div>
                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

		
		<!-- Add message -->
		<jsp:include page="/Admin/pages/popups/logout.html" /> 

        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- jQuery UI 1.10.3 -->
        <script src="/Admin/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="/Admin/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- Morris.js charts -->
        <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="/Admin/js/plugins/morris/morris.min.js" type="text/javascript"></script>
        <!-- Sparkline -->
        <script src="/Admin/js/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
        <!-- jvectormap -->
        <script src="/Admin/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
        <script src="/Admin/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
        <!-- fullCalendar -->
        <script src="/Admin/js/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
        <!-- jQuery Knob Chart -->
        <script src="/Admin/js/plugins/jqueryKnob/jquery.knob.js" type="text/javascript"></script>
        <!-- daterangepicker 
        <script src="/Admin/js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
        -->
        <!-- Bootstrap WYSIHTML5 -->
        <script src="/Admin/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js" type="text/javascript"></script>
        <!-- iCheck -->
        <script src="/Admin/js/plugins/iCheck/icheck.min.js" type="text/javascript"></script>

        <!-- AdminLTE App -->
        <script src="/Admin/js/AdminLTE/app.js" type="text/javascript"></script>
        
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="/Admin/js/AdminLTE/dashboard.js" type="text/javascript"></script>        

    </body>
</html>