<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="model.Usuario"%>
<%@ page import="model.Producto" %>
<%@ page import="model.Servicio" %>
<%@ page import="java.util.List"%>
<%@ page session="true" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Luna Technology</title>

    <!-- Bootstrap Core CSS -->
    <link href="/User/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="/User/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="/User/vendor/font-awesome/css/minified.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Theme CSS -->
    <link href="/User/css/agency.min.css" rel="stylesheet">
</head>
<body id="page-top" class="index">

    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">Luna 	 Technology</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#services">Servicios</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#products">Productos </a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#tienda">Tienda</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#about">Sobre Nosotros</a>
                    </li> 
                    <li>
                        <a class="page-scroll" href="#help">Ayuda</a>
                    </li> 
                    <li>
                        <a class="page-scroll" href="#contact">Contáctanos</a>
                    </li>
                    <% String email = (String)request.getSession().getAttribute("email"); %>
                    <% if(email == null){ %>
                    <li>
                        <a class="page-scroll" data-toggle="modal"data-target="#myModal" style="cursor:pointer;">Ingreso/Registro</a>
                    </li>
                    <% }else{%>
                     <li>
                        <a class="page-scroll" data-toggle="modal" data-target="#logout" style="cursor:pointer;"><%= email %></a>
                    </li>
                    <%} %>
                    
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
    <% String message = (String)request.getSession().getAttribute("message"); %>
    
    <% if(message!=null){ %>
	    <div class="alert alert-warning alert-dismissable" style="position:fixed;top:90px;right:5px;">
	        <i class="fa fa-warning"></i>
	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	         <%= message %>
	    </div>
	<%}else{%>
		<% message = (String)request.getAttribute("message"); %>
		<% if(message!=null){ %>
			<div class="alert alert-warning alert-dismissable" style="position:fixed;top:90px;right:5px;">
		        <i class="fa fa-warning"></i>
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		         <%= message %>
		    </div>
		    <%} %>
	<%} %>
	<!-- logout -->
	<div class="modal fade" id="logout" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	    <div class="modal-dialog modal-lg">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
	                    X
	                </button>
	                <h4 class="modal-title" id="tit">
	                    ¿Está seguro que desea salir?
	                </h4>
	            </div>
	            <div class="modal-body" style="height:60px;">
	            	<a class="close" href="/salir" style="opacity:1;">Salir</a>
	            	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="opacity:1;padding-right:40px;">
	                    Cancelar
	                </button>
	            </div>
	        </div>
	    </div>
	</div>
	
    <!-- Login and Register Modal -->
    
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
	    aria-hidden="true">
	    <div class="modal-dialog modal-lg">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
	                    X
	                </button>
	                <h4 class="modal-title" id="myModalLabel">
	                    Ingreso/Registro 
	                </h4>
	            </div>
	            <div class="modal-body">
	                <div class="row">
	                    <div class="col-md-8" style="border-right: 1px dotted #C2C2C2;padding-right: 30px;">
	                        <!-- Nav tabs -->
	                        <ul class="nav nav-tabs">
	                            <li class="active"><a href="#Login" data-toggle="tab">Ingreso</a></li>
	                            <li><a href="#Registration" data-toggle="tab">Registro</a></li>
	                        </ul>
	                        <!-- Tab panes -->
	                        <div class="tab-content" style="margin-top:20px;">
	                            <div class="tab-pane active" id="Login">
	                                <form role="form" class="form-horizontal" action="/ingreso" method="post">
		                                <div class="form-group">
		                                    <label for="email" class="col-sm-2 control-label">
		                                        Correo</label>
		                                    <div class="col-sm-10">
		                                        <input type="email" name="email" class="form-control" id="email1" placeholder="Correo" />
		                                    </div>
		                                </div>
		                                <div class="form-group">
		                                    <label for="exampleInputPassword1" class="col-sm-2 control-label">
		                                        Contraseña</label>
		                                    <div class="col-sm-10">
		                                        <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Contraseña" />
		                                    </div>
		                                </div>
		                                <div class="row">
		                                    <div class="col-sm-2">
		                                    </div>
		                                    <div class="col-sm-10">
		                                        <input type="submit" class="btn btn-primary btn-sm" value="Enviar" />
		                                        <a href="javascript:;">¿Olvidó su contraseña?</a>
		                                    </div>
		                                </div>
	                                </form>
	                            </div>
	                            <div class="tab-pane" id="Registration">
	                                <form role="form" class="form-horizontal" action="/registro" method="post">
	                                
		                                <div class="form-group">
		                                    <label for="name" class="col-sm-2 control-label">
		                                        Nombre</label>
		                                    <div class="col-sm-10">
	                                            <input type="text" name="name" class="form-control" placeholder="Nombre" />	                                          
		                                    </div>
		                                </div>
		                                
		                                <div class="form-group">
		                                    <label for="email" class="col-sm-2 control-label">
		                                        Correo</label>
		                                    <div class="col-sm-10">
		                                        <input type="email" name="email" class="form-control" id="email" placeholder="Correo" />
		                                    </div>
		                                </div>
		  
		                                <div class="form-group">
		                                    <label for="password" class="col-sm-2 control-label">
		                                        Contraseña</label>
		                                    <div class="col-sm-10">
		                                        <input type="password" class="form-control" name="password" id="password" placeholder="Contraseña" />
		                                    </div>
		                                </div>
		                                <input type="text" name="type" vlaue="Usuario" style="display:none;" />
		                                <input name="interface" type="text" style="display:none;" value="user">
		                                <div class="row">
		                                    <div class="col-sm-2">
		                                    </div>
		                                    <div class="col-sm-10">
		                                        <input type="submit" class="btn btn-primary btn-sm" value="Guardar & Continuar" />
		                                        <button type="button" class="btn btn-default btn-sm">
		                                            Cancelar</button>
		                                    </div>
		                                </div>
	                                </form>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="col-md-4">
	                        <div class="row text-center sign-with">
	                            <div class="col-md-12">
	                                <h3>
	                                    Ingresar con</h3>
	                            </div>
	                            <div class="col-md-12">
	                                <div class="btn-group btn-group-justified">
	                                    <a href="#" class="btn btn-primary">Facebook</a> <a href="#" class="btn btn-danger">
	                                        Google</a>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
	

    <!-- Header -->
    <header>
        <div class="container">
            <div class="intro-text">
                <div class="intro-lead-in"><b>FERRETERIA LUNA</b></div>
                <a href="#services" class="page-scroll btn btn-xl">A por ello</a>
            </div>
        </div>
    </header>
	
	<% List<Servicio> servicios = (List<Servicio>) request.getAttribute("servicios"); %>	
	
	<% List<Producto> productos = (List<Producto>) request.getAttribute("productos"); %>
	
    <!-- Services Section -->
    <section id="services" style="padding-top:50px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Services</h2>
                    <h3 class="section-subheading text-muted">Encuentra lo mejor</h3>
                </div>
            </div>
            <div class="row text-center">
            <% for (Servicio s : servicios){%>		
                <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading"><%=s.getNombre()%></h4>
                    <p class="text-muted"><%=s.getDescripcion()%></p>
                </div>
			<%}%>  
            </div>
        </div>
    </section>

    <!-- Portfolio Grid Section -->
    <section id="products" class="bg-light-gray" style="padding-top:50px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Top Productos</h2>
                    <h3 class="section-subheading text-muted">Nuestros productos más vendidos.</h3>
                </div>
            </div>
            <div class="row">
            	<% for (Producto p : productos){%>
                <div class="col-md-3 col-sm-6 portfolio-item" style="height:490px">
                    <a href="#portfolioModal<%=p.getCodigo()%>" class="portfolio-link" data-toggle="modal">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="/Admin/img/productos/<%=p.getFoto1()%>" class="img-responsive" alt="">
                        <!--<div class="hover">
                         <ul class="icons unstyled">
							<li>
								<div class="circle ribbon ribbon-sale">Sale</div>
							</li>
							<li>
								<a href="#" class="circle add-to-cart"><i class="iconfont-shopping-cart"></i></a>
							</li>
						</ul>
						</div> -->
                    </a>
                    <div class="portfolio-caption">
                        <h4><%=p.getNombre()%></h4>
                        <p class="text-muted"><%=p.getDescripcion()%></p>
                    </div>
                </div>
                <!-- Portfolio Modal 1 -->
			    <div class="portfolio-modal modal fade" id="portfolioModal<%=p.getCodigo()%>" tabindex="-1" role="dialog" aria-hidden="true">
			        <div class="modal-dialog">
			            <div class="modal-content">
			                <div class="close-modal" data-dismiss="modal">
			                    <div class="lr">
			                        <div class="rl">
			                        </div>
			                    </div>
			                </div>
			                <div class="container">
			                    <div class="row">
			                        <div class="col-lg-8 col-lg-offset-2">
			                            <div class="modal-body">
			                                <!-- Project Details Go Here -->
			                                <h2><%=p.getNombre()%></h2>
			                                
			                                <img class="img-responsive img-centered" src="/Admin/img/productos/<%=p.getFoto1()%>" alt="">
			                                <p class="item-intro text-muted"><%=p.getDescripcion()%></p>
			                                <p class="item-intro text-muted">MARCA: <%=p.getMarca()%></p>
			                                <p class="item-intro text-muted">PRECIO: <%=p.getPrecio1()%></p>
			                                <p class="item-intro text-muted">PRECIO OFERTA: <%=p.getPrecio2()%></p>
			                                
			                                <button type="button" class="btn btn-success"><i class="fa fa-cart-plus"></i> Comprar</button>
			                                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>
                <%}%>
            </div>
        </div>
    </section>



    <!-- Team Section -->
    <section id="tienda" class="bg-light-gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Tienda</h2>
                    <h3 class="section-subheading text-muted">Nuevos productos disponibles en todas nuestras tiendas.</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4">
                    <div class="team-member">
                        <img src="/User/img/team/1.jpg" class="img-responsive img-circle" alt="">
                        <h4>Kay Garland</h4>
                        <p class="text-muted">Lead Designer</p>
                        <ul class="list-inline social-buttons">
                            <li><a href="#"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="team-member">
                        <img src="/User/img/team/2.jpg" class="img-responsive img-circle" alt="">
                        <h4>Larry Parker</h4>
                        <p class="text-muted">Lead Marketer</p>
                        <ul class="list-inline social-buttons">
                            <li><a href="#"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="team-member">
                        <img src="/User/img/team/3.jpg" class="img-responsive img-circle" alt="">
                        <h4>Diana Pertersen</h4>
                        <p class="text-muted">Lead Developer</p>
                        <ul class="list-inline social-buttons">
                            <li><a href="#"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <p class="large text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut eaque, laboriosam veritatis, quos non quis ad perspiciatis, totam corporis ea, alias ut unde.</p>
                </div>
            </div>
        </div>
    </section>

	    <!-- About Section -->
    <section id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Sobre Nosotros</h2>
                    <h3 class="section-subheading text-muted">Somos una empresa dedicada a la venta de productos de ferreteria.
					</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <ul class="timeline">
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="/User/img/about/1.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4 class="subheading">Misión</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">Somos una empresa comercializadora de productos de ferretería liviana que satisfacen las necesidades de nuestros clientes, con asesoría, calidad y respaldo.
Actuamos basados en nuestros valores corporativos, preservando el sano equilibrio entre los intereses de clientes, colaboradores, proveedores, accionistas y comunidad donde operamos.</p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="/User/img/about/2.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4 class="subheading">Visión</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">En el año 2020 seremos la empresa ferretera preferida por nuestros clientes, brindándoles soluciones, desarrollando el talento humano de nuestros colaboradores y generando beneficios para los accionistas.</p>
                                </div>
                            </div>
                        </li>
                        <!-- 
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="/User/img/about/3.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>December 2012</h4>
                                    <h4 class="subheading">Transition to Full Service</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>
                                </div>
                            </div>
                        </li>-->
                        <!-- 
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="/User/img/about/4.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>July 2014</h4>
                                    <h4 class="subheading">Phase Two Expansion</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>
                                </div>
                            </div>
                        </li> -->
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <h4>Atencion <br>24 horas <br>al diá!</h4>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!-- Clients Aside -->
    <aside class="clients">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <a href="#">
                        <img src="/User/img/logos/envato.jpg" class="img-responsive img-centered" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-sm-6">
                    <a href="#">
                        <img src="/User/img/logos/designmodo.jpg" class="img-responsive img-centered" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-sm-6">
                    <a href="#">
                        <img src="/User/img/logos/themeforest.jpg" class="img-responsive img-centered" alt="">
                    </a>
                </div>
                <div class="col-md-3 col-sm-6">
                    <a href="#">
                        <img src="/User/img/logos/creative-market.jpg" class="img-responsive img-centered" alt="">
                    </a>
                </div>
            </div>
        </div>
    </aside>
    
    
     <!-- Help Section -->
    <section id="help">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Videos y Repositorio</h2>
                </div>
            </div>
            <div>
             <ul class="treeview-menu">
             	   <li><a href="#"><i class="fa fa-angle-double-right"></i> Video Explicacion Requerimiento </a></li>
                   <li><a href="https://youtu.be/NHWbSpjKqwU"><i class="fa fa-angle-double-right"></i> Video Funcionalidad (español) </a></li>
                   <li><a href="https://youtu.be/k3PbFuGe7Ow"><i class="fa fa-angle-double-right"></i> Video Funcionalidad (ingles)</a></li>
                   <li><a href="https://gitlab.com/mmiranda17/PW2.SGFL"><i class="fa fa-angle-double-right"></i> Repositorio</a></li>
            </ul>
            </div>
        </div>
    </section>
    

    <!-- Contact Section -->
    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Contactanos</h2>
                    <h3 class="section-subheading text-muted">¡Suscribete y conoce sobre ventas, eventos y ofertas exclusivas!.</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <form name="sentMessage" id="contactForm" novalidate>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Your Name *" id="name" required data-validation-required-message="Please enter your name.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Your Email *" id="email" required data-validation-required-message="Please enter your email address.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input type="tel" class="form-control" placeholder="Your Phone *" id="phone" required data-validation-required-message="Please enter your phone number.">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <textarea class="form-control" placeholder="Your Message *" id="message" required data-validation-required-message="Please enter a message."></textarea>
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 text-center">
                                <div id="success"></div>
                                <button type="submit" class="btn btn-xl">Enviar Mensaje</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <span class="copyright">Copyright &copy; Ferreteria Luna 2018</span>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline social-buttons">
                        <li><a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline quicklinks">
                        <li><a href="#">Privacy Policy</a>
                        </li>
                        <li><a href="#">Terms of Use</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>



    <!-- jQuery -->
    <script src="/User/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="/User/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js" integrity="sha384-mE6eXfrb8jxl0rzJDBRanYqgBxtJ6Unn4/1F7q4xRRyIw7Vdg9jP4ycT7x1iVsgb" crossorigin="anonymous"></script>

    <!-- Contact Form JavaScript -->
    <script src="/User/js/jqBootstrapValidation.js"></script>
    <script src="/User/js/contact_me.js"></script>

    <!-- Theme JavaScript -->
    <script src="/User/js/agency.min.js"></script>

</body>
</html>